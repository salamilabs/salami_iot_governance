"""salami_iot_governance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from tastypie.api import Api

from SalamiGovernance import views
from SalamiGovernance.api import KitchenResource, ApiKeyResource, DeviceResource, UserResource

salami_api = Api(api_name="v1")
salami_api.register(KitchenResource())
salami_api.register(ApiKeyResource())
salami_api.register(DeviceResource())
salami_api.register(UserResource())

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^api/', include(salami_api.urls)),
    url(r'^verify/', views.verify_key),
    url(r'^upload/', views.upload_passport),
]

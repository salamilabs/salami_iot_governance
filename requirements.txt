Django==1.9.4
django-braces==1.8.1
django-cors-headers==1.1.0
django-oauth-toolkit-fork==0.11.0
django-separatedvaluesfield==0.3.3
django-tastypie==0.13.3
mysqlclient==1.3.7
oauthlib==1.0.1
pycrypto==2.6.1
python-dateutil==2.5.1
python-mimeparse==1.5.1
shortuuid==0.4.3
six==1.10.0

from django.contrib import admin

from SalamiGovernance.models import Kitchen, ApiKey, Device


class KitchenAdmin(admin.ModelAdmin):
    pass


class ApiKeyAdmin(admin.ModelAdmin):
    pass


class DeviceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Kitchen, KitchenAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)
admin.site.register(Device, DeviceAdmin)

from django.apps import AppConfig


class SalamigovernanceConfig(AppConfig):
    name = 'SalamiGovernance'

import hashlib
from datetime import datetime

from Crypto import Random


def generate_key(fixed1, fixed2):
    random = Random.new()
    random_part = random.read(16)
    timestamp = datetime.now().microsecond
    key = fixed1 + str(random_part) + str(timestamp) + fixed2
    return hashlib.sha224(str(key).encode("utf-8")).hexdigest()

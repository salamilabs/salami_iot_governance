from django.db import models
from django.dispatch import receiver, Signal

from SalamiGovernance.keygenerator import generate_key


class ApiKey(models.Model):
    api_key = models.CharField(max_length=32, null=True, blank=True)
    status = models.SmallIntegerField(choices=((0, 'Revoked'), (1, 'Active')), default=0)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.deleted:
            key_deleted.send(sender=self.__class__, key_id=self.id)

        super(ApiKey, self).save(*args, **kwargs)

    def __str__(self):
        return u'Api key: %s' % self.api_key


class Kitchen(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    owner = models.ForeignKey('auth.User', related_name='kitchens')
    api_key = models.ForeignKey('ApiKey', related_name='keys', null=True, blank=True)
    status = models.SmallIntegerField(choices=((0, 'Disabled'), (1, 'Enabled')), default=0)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.api_key is None:
            key = generate_key(self.name, self.owner.username)
            api_key = ApiKey()
            api_key.status = 1
            api_key.api_key = key
            api_key.save()
            self.api_key = api_key

        if self.deleted:
            kitchen_deleted.send(sender=self.__class__, api_key_id=self.api_key.id)
        super(Kitchen, self).save(*args, **kwargs)

    def __str__(self):
        return u'Name: %s, Owner: %s' % (self.name, self.owner)


class Device(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=255, null=True, blank=True)
    api_key = models.ForeignKey('ApiKey', related_name='devices')
    passport = models.FilePathField(path="/opt/salamiiot/")
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return u'Name: %s, Api key: %s' % (self.name, self.api_key.api_key)


# Signals
kitchen_deleted = Signal(providing_args=['kitchen_id'])
key_deleted = Signal(providing_args=['key_id'])


@receiver(key_deleted, sender=ApiKey)
def key_cascade_delete(sender, **kwargs):
    key = kwargs['key_id']
    Device.objects.filter(api_key__pk=key).update(deleted=True)


@receiver(kitchen_deleted, sender=Kitchen)
def kitchen_cascade_delete(sender, **kwargs):
    api_id = kwargs['api_key_id']
    api_key = ApiKey.objects.get(pk=api_id)
    api_key.deleted = True
    api_key.save()

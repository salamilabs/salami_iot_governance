# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-24 00:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SalamiGovernance', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apikey',
            name='api_key',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]

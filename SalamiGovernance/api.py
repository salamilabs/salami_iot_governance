from django.contrib.auth.models import User
from tastypie import fields
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from SalamiGovernance.authentication import OAuth20Authentication
from SalamiGovernance.authorization import UserObjectsOnlyAuthorization
from SalamiGovernance.models import Kitchen, ApiKey, Device


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        excludes = ['resource_uri', 'date_joined', 'password', 'is_staff', 'is_superuser']
        filtering = {
            'username': ALL,
        }

    def hydrate(self, bundle):
        if 'raw_password' in bundle.data:
            u = User(username='dummy')
            u.set_password(bundle.data['raw_password'])
            bundle.data['password'] = u.password
        return bundle

    def dehydrate(self, bundle):
        # del bundle.data['password']
        return bundle


class ApiKeyResource(ModelResource):
    class Meta:
        queryset = ApiKey.objects.filter(deleted=False)
        resource_name = 'api_key'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        excludes = ['resource_uri']
        filtering = {
            'api_key': ALL,

        }


class KitchenResource(ModelResource):
    owner = fields.ForeignKey(UserResource, 'owner', full=True)
    api_key = fields.ForeignKey(ApiKeyResource, 'api_key', full=True)

    class Meta:
        queryset = Kitchen.objects.filter(deleted=False)
        resource_name = 'kitchen'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        filtering = {
            'name': ALL,
            'owner': ALL_WITH_RELATIONS,
            'api_key': ALL_WITH_RELATIONS

        }


class MultipartResource(object):
    def deserialize(self, request, data, format=None):
        if not format:
            format = request.META.get('CONTENT_TYPE', 'application/json')
        if format == 'application/x-www-form-urlencoded':
            return request.POST
        if format.startswith('multipart'):
            data = request.POST.copy()
            data.update(request.FILES)
            return data
        return super(MultipartResource, self).deserialize(request, data, format)


class DeviceResource(ModelResource, MultipartResource):
    api_key = fields.ForeignKey(ApiKeyResource, 'api_key', full=True)

    class Meta:
        queryset = Device.objects.filter(deleted=False)
        resource_name = 'device'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        filtering = {
            'api_key': ALL_WITH_RELATIONS,
            'name': ALL,
        }
        ordering = ['name']

import os

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from SalamiGovernance.models import ApiKey

UPLOAD_FOLDER = "/opt/salamiiot/"


def verify_key(request):
    request_key = request.GET.get('k', '')
    key = ApiKey.objects.filter(api_key=request_key, status=1, deleted=False)

    if len(key) is not 0:
        return HttpResponse(status=200)

    return HttpResponse(status=404)


@csrf_exempt
def upload_passport(request):
    if request.method == 'POST':
        passport = request.FILES['passport.salami']
        suffix = request.POST.get("suffix")
        key = ApiKey.objects.filter(api_key=suffix, status=1, deleted=False)
        if len(key) > 0:
            file_path = os.path.join(UPLOAD_FOLDER, "salami.passport." + suffix)
            with open(os.path.join(file_path), 'wb+') as destination:
                for chunk in passport.chunks():
                    destination.write(chunk)
            return HttpResponse(file_path, status=201)
        return HttpResponse(status=400)
